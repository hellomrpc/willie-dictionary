package com.willie.dictionary;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.willie.dictionary.businessImpl.WordBusinessImpl;
import com.willie.dictionary.common.DicItem;
import com.willie.dictionary.common.DicItemComparator;
import com.willie.dictionary.common.Phoneme;
import com.willie.dictionary.common.WordUtils;
import com.willie.dictionary.entity.Word;
import org.junit.Test;

import javax.annotation.Resource;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Export extends ApplicationTest {

    @Resource
    private WordBusinessImpl wordBusiness;

    @Test
    public void run() {
        List<Word> validWords = wordBusiness.getValidWords();
        List<DicItem> dicItems = new ArrayList<>();
        for (Word word : validWords) {
            try {
                String mw = WordUtils.parseMw(word);
                String[] mws = WordUtils.splitMw(mw);
                Phoneme[] vowels = Phoneme.convertMwsToVowels(mws);
                Phoneme[][] phonemes = Phoneme.convertMwsToPhonemesArray(mws);
                DicItem dicItem = new DicItem();
                dicItem.setWord(word.getWord());
                dicItem.setMws(mws);
                dicItem.setVowels(vowels);
                dicItem.setPhonemes(phonemes);
                dicItem.setMeaning(word.getCollins());
                dicItems.add(dicItem);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        List<DicItem> collect = dicItems.stream().sorted(new DicItemComparator()).collect(Collectors.toList());
        StringBuilder sb = new StringBuilder();
        final char SEPARATE = '\t';
        for (int i = 0; i < collect.size(); i++) {
            DicItem dicItem = collect.get(i);
            sb.append(dicItem.getWord())
                    .append(SEPARATE)
                    .append(StringUtils.isBlank(String.join("-", dicItem.getMws())) ? "NULL" : String.join("-", dicItem.getMws()))
                    .append(SEPARATE)
                    .append(hide(dicItem.getMeaning(), dicItem.getWord()))
                    .append(SEPARATE)
                    .append(i);
            if(i != collect.size() - 1)
                sb.append('\n');
        }

        String projectPath = System.getProperty("user.dir");
        File file = new File(projectPath, "word");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(sb.toString());
        } catch(IOException e) {
           e.printStackTrace();
        }

        // writeToTxt(collect);
    }

    private String hide(String meaning, String word) {
        Pattern pattern = Pattern.compile("[A-Za-z\\-]+");
        Matcher matcher = pattern.matcher(meaning);
        while(matcher.find()) {
            String w = matcher.group();
            if(w.toLowerCase().contains(word.toLowerCase()) && w.length() < word.length() + 3) {
                meaning = meaning.replace(w, "***");
            }
        }
        return meaning;
    }

/*    private static void writeToTxt(List<WordClass> wordClasses) {
        String projectPath = System.getProperty("user.dir");
        File root = new File(projectPath, "word");
        if(!root.exists()) {
            root.mkdir();
        } else if (root.isFile())
            throw new RuntimeException("There is an file named \"word\" in " + projectPath);
        else if(root.isDirectory()) {
            for (File file : root.listFiles())
                file.delete();
        }
        for (int i = 0; i < wordClasses.size(); i++) {
            File f = new File(root, String.valueOf(i));
            Phoneme[] vowels = wordClasses.get(i).getVowels();
            TreeSet<DicItem> words = wordClasses.get(i).getWords();
            try(BufferedWriter bw = new BufferedWriter(new FileWriter(f))) {
                StringBuilder sb = new StringBuilder();
                sb.append("+++++++ ");
                for (int j = 0; j < vowels.length; j++) {
                    Phoneme vowel = vowels[j];
                    if(vowel == null)
                        sb.append("null");
                    else
                        sb.append(vowel.getPhoneme());
                    if(j != vowels.length - 1)
                        sb.append("-");
                }
                sb.append(" +++++++\n\n");
                Iterator<DicItem> it = words.iterator();
                int j = 0;
                while(it.hasNext()) {
                    DicItem dicItem = it.next();
                    String word = dicItem.getWord();
                    String[] mws = dicItem.getMws();
                    String meaning = dicItem.getMeaning();
                    sb.append(word)
                            .append("\t\t")
                            .append(String.join("-", mws))
                            .append('\n')
                            .append(meaning)
                            .append('\n');
                    j++;
                    if(j != words.size())
                        sb.append("==============").append('\n');
                }
                bw.write(sb.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }*/
}
