package com.willie.dictionary;

import com.willie.dictionary.businessImpl.WordBusinessImpl;
import com.willie.dictionary.common.WordUtils;
import com.willie.dictionary.entity.Word;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
public class PhonemeTest extends ApplicationTest{
    @Resource
    private WordBusinessImpl wordBusiness;

    @Test
    public void run() {
        List<Word> validWords = wordBusiness.getValidWords();
        Set<Character> phonemes = new HashSet<>();
        for (Word word : validWords) {
            try {
                String mw = WordUtils.parseMw(word);
                String[] mws = WordUtils.splitMw(mw);
                String mw1 = String.join("-", mws);
                if(mw1.contains("t\u035fh")) {
                    System.out.println("\"" + word.getWord() + "\": " + mw1);
                }
                char[] chars = mw1.toCharArray();
                for (char c : chars) {
                    phonemes.add(c);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        for (Character phoneme : phonemes) {
            String unicode = String.format("%04x", (int) phoneme);
            System.out.println("\" "+ phoneme +" \" 的Unicode码是: \\u" + unicode);
        }

    }
}