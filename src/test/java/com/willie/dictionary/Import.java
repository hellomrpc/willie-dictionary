package com.willie.dictionary;

import com.willie.dictionary.business.WordBusiness;
import com.willie.dictionary.entity.Word;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


@Slf4j
public class Import {
    @Resource
    private WordBusiness wordBusiness;

    @Test
    public void run() {
        InputStream resource = Import.class.getResourceAsStream("/words.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
        try {
            String word;
            int count = 0;
            while((word = reader.readLine()) != null) {
                if(word.trim().length() > 0) {
                    if(wordBusiness.getById(word) != null) {
                        log.warn("'{}' has been exist", word);
                        continue;
                    }
                    Word w = new Word();
                    w.setWord(word);
                    boolean save = wordBusiness.save(w);
                    if(!save) {
                        log.warn("save word fail, parameter: {}", word);
                        continue;
                    }
                    count++;
                    if(count % 100 == 0)
                        log.info("{} words has been inserted", count);
                }
            }
            log.info("All {} words has been inserted", count);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
