import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CodeGenerator {


    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://47.100.23.130:3306/dictionary?useUnicode=true&useSSL=false&characterEncoding=utf8");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("caiwl123");
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath);
        gc.setAuthor("willie");
        gc.setOpen(false);
        gc.setMapperName("%sDao");
        gc.setServiceName("%sBusiness");
        gc.setServiceImplName("%sBusinessImpl");
        mpg.setGlobalConfig(gc);

        // 模板配置
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setController(null);
        mpg.setTemplate(templateConfig);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(null);
        pc.setEntity("com.willie.dictionary.entity");
        pc.setMapper("com.willie.dictionary.dao");
        pc.setService("com.willie.dictionary.business");
        pc.setServiceImpl("com.willie.dictionary.businessImpl");
        pc.setXml("mapper");
        Map<String, String> pathInfo = new HashMap<>();
        pathInfo.put(ConstVal.ENTITY_PATH, joinPath(gc.getOutputDir() + "/src/main/java", pc.getEntity()));
        pathInfo.put(ConstVal.MAPPER_PATH, joinPath(gc.getOutputDir() + "/src/main/java", pc.getMapper()));
        pathInfo.put(ConstVal.XML_PATH, joinPath(gc.getOutputDir() + "/src/main/resources", pc.getXml()));
        pathInfo.put(ConstVal.SERVICE_PATH, joinPath(gc.getOutputDir() + "/src/main/java", pc.getService()));
        pathInfo.put(ConstVal.SERVICE_IMPL_PATH, joinPath(gc.getOutputDir() + "/src/main/java", pc.getServiceImpl()));
        pc.setPathInfo(pathInfo);
        mpg.setPackageInfo(pc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true);
        strategy.setEntityColumnConstant(true);
        strategy.setEntityBooleanColumnRemoveIsPrefix(true);
        strategy.setEntityTableFieldAnnotationEnable(true);
        strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
        strategy.setTablePrefix("t_");
        mpg.setStrategy(strategy);
        mpg.execute();
    }

    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入" + tip + "：");
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotEmpty(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    private static String joinPath(String parentDir, String packageName) {
        if (StringUtils.isBlank(parentDir)) {
            parentDir = System.getProperty(ConstVal.JAVA_TMPDIR);
        }
        if (!StringUtils.endsWith(parentDir, File.separator)) {
            parentDir += File.separator;
        }
        packageName = packageName.replaceAll("\\.", StringPool.BACK_SLASH + File.separator);
        return parentDir + packageName;
    }
}