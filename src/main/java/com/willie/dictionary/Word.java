package com.willie.dictionary;

public class Word {
    private String word;
    private String hw;
    private String[] mws;
    private String meaning;


    public String getHw() {
        return hw;
    }

    public void setHw(String hw) {
        this.hw = hw;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String[] getMws() {
        return mws;
    }

    public void setMws(String[] mws) {
        this.mws = mws;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }
}
