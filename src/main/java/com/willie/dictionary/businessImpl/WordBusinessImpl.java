package com.willie.dictionary.businessImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.willie.dictionary.business.WordBusiness;
import com.willie.dictionary.dao.WordDao;
import com.willie.dictionary.entity.Word;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author willie
 * @since 2020-02-15
 */
@Service
@Slf4j
public class WordBusinessImpl extends ServiceImpl<WordDao, Word> implements WordBusiness {

    private OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS).build();

    // period one hour
    @Scheduled(cron = "0 0 0/1 * * ?")
    public void lookupWebster() {
        String template = "https://www.dictionaryapi.com/api/v3/references/collegiate/json/%s?key=bb8dc165-4ab0-420d-ac7c-efc55efafd07";
        Page<Word> page = page(new Page<>(1, 40), Wrappers.lambdaQuery(Word.class).isNull(Word::getWebster));
        List<Word> records = page.getRecords();
        int count = 0;
        for (Word record : records) {
            Request request = new Request.Builder()
                    .url(String.format(template, record.getWord()))
                    .build();
            try (Response response = client.newCall(request).execute()) {
                String content = response.body().string();
                if(StringUtils.isNotBlank(content)) {
                    record.setWebster(content);
                    count++;
                    if(count % 5 == 0)
                        log.info("{} words has been updated webster", count);
                } else {
                    log.warn("Word's webster is not exist, parameter: {}", record.getWord());
                    record.setWebster("null");
                }
                updateById(record);
            } catch (IOException e) {
                log.warn("There is an err: {}, parameter: {}", e.getMessage(), record.getWord());
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Scheduled(cron = "0 30 0/1 * * ? ")
    public void lookupCollins() {
        String template = "https://www.collinsdictionary.com/us/dictionary/english/%s";
        Page<Word> page = page(new Page<>(1, 40), Wrappers.lambdaQuery(Word.class).isNull(Word::getCollins));
        List<Word> records = page.getRecords();
        int count = 0;
        for (Word record : records) {
            Request request = new Request.Builder()
                    .url(String.format(template, record.getWord()))
                    .build();
            try (Response response = client.newCall(request).execute()) {
                String content = response.body().string();
                if(StringUtils.isNotBlank(content)) {
                    Document doc = Jsoup.parse(content);
                    Element def = doc.select("div.dictionary.Cob_Adv_US.dictentry div.content.definitions.cobuild.am > div.hom > div.sense > div.def").first();
                    if(def != null) {
                        record.setCollins(def.text());
                        count++;
                        if(count % 5 == 0)
                            log.info("{} words has been updated collins", count);
                    } else {
                        log.warn("Words collins's meaning is not exist, parameter: {}", record.getWord());
                        record.setCollins("null");
                    }
                } else {
                    log.info("Word's collins is not exist, parameter: {}", record.getWord());
                    record.setCollins("null");
                }
                updateById(record);
            } catch (IOException e) {
                log.warn("There is an err: {}, parameter: {}", e.getMessage(), record.getWord());
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public List<Word> getValidWords() {
        List<Word> ret = new ArrayList<>();
        LambdaQueryWrapper<Word> wrapper = Wrappers.lambdaQuery(Word.class)
                .isNotNull(Word::getWebster)
                .and(w -> w.ne(Word::getWebster, "null"))
                .and(w -> w.isNotNull(Word::getCollins))
                .and(w -> w.ne(Word::getCollins, "null"));
        int count = count(wrapper);
        int pageIndex = 0;
        final int PAGE_SIZE = 200;
        do {
            Page<Word> page = page(new Page<>(++pageIndex, PAGE_SIZE), wrapper);
            ret.addAll(page.getRecords());
        } while (pageIndex * PAGE_SIZE < count);
        return ret;
    }
}
