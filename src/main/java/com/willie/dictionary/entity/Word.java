package com.willie.dictionary.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author willie
 * @since 2020-02-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_word")
public class Word implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId("word")
    private String word;

    @TableField("webster")
    private String webster;

    @TableField("collins")
    private String collins;


    public static final String WORD = "word";

    public static final String WEBSTER = "webster";

    public static final String COLLINS = "collins";

}
