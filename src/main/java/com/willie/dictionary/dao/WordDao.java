package com.willie.dictionary.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.willie.dictionary.entity.Word;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author willie
 * @since 2020-02-15
 */
public interface WordDao extends BaseMapper<Word> {

}
