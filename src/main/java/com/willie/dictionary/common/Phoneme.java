package com.willie.dictionary.common;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

@Getter
public enum Phoneme {

    LONG_A("ā", 1, true),
    LONG_E("ē", 2, true),
    SHORT_I("i", 3, true),
    LONG_I("ī", 4, true),
    LONG_O("ō", 5, true),
    LONG_U("yü", 6, true),
    SHORT_U("yə", 7, true),
    SHORT_A("a", 8, true),
    SHORT_E("e", 9, true),
    DOT_LONG_U("ü", 10, true),
    DOT_SHORT_U("u\u0307", 11, true),
    DOT_LONG_A("ä", 12, true),
    DOT_SHORT_O("ȯ", 13, true),
    COMB_SHORT_A_DOT_SHORT_U("au\u0307", 14, true),
    COMB_DOT_SHORT_O_SHORT_I("ȯi", 15, true),
    SCHWA("ə", 16, true),
    SCHWA_L1("əl", 17, true),
    SCHWA_L2("ᵊl", 18, true),
    SCHWA_N("ᵊn", 19, true),
    B("b", 20, false),
    P("p", 21, false),
    D("d", 22, false),
    T("t", 23, false),
    G("g", 24, false),
    K("k", 25, false),
    F("f", 26, false),
    V("v", 27, false),
    M("m", 28, false),
    N1("n", 29, false),
    N2("ŋ", 30, false),
    H("h", 31, false),
    L("l", 32, false),
    J("j", 33, false),
    CH("ch", 34, false),
    SH("sh", 35, false),
    ZH("zh", 36, false),
    S("s", 37, false),
    Z("z", 38, false),
    TH1("th", 39, false),
    TH2("t\u035fh", 40, false),
    Y("y", 41, false),
    W("w", 42, false),
    R("r", 43, false),;

    private String phoneme;
    private int order;
    private boolean isVowel;
    Phoneme(String phoneme, int order, boolean isVowel) {
        this.phoneme = phoneme;
        this.order = order;
        this.isVowel = isVowel;
    }

    public static Phoneme[] find(String mw) {
        int nextIndex = 0;
        List<Phoneme> phonemes = new ArrayList<>();
        if(mw.startsWith("ˈ") || mw.startsWith("ˌ"))
            mw = mw.substring(1);
        while(mw.length() > nextIndex) {
            String substring = mw.substring(nextIndex);
            int len = 0;
            Phoneme p = null;
            for (Phoneme phoneme : Phoneme.values()) {
                if(substring.startsWith(phoneme.getPhoneme())) {
                    if (phoneme.getPhoneme().length() > len) {
                        len = phoneme.getPhoneme().length();
                        p = phoneme;
                    }
                }
            }
            if(p == null)
                break;
            nextIndex += len;
            phonemes.add(p);
        }
        return phonemes.toArray(new Phoneme[0]);
    }

    public static boolean compare(Phoneme[] phoneme1, Phoneme[] phoneme2) {
        if (phoneme1.length != phoneme2.length)
            return false;
        for (int i = 0; i < phoneme1.length; i++) {
            Phoneme vowel1 = phoneme1[i];
            Phoneme vowel2 = phoneme2[i];
            if(vowel1 != vowel2)
                return false;
        }
        return true;
    }

    public static Phoneme[][] convertMwsToPhonemesArray(String[] mws) {
        Phoneme[][] phonemesArray = new Phoneme[mws.length][];
        for (int i = 0; i < mws.length; i++) {
            phonemesArray[i] = Phoneme.find(mws[i]);
        }
        return phonemesArray;
    }

    public static Phoneme[] convertMwsToVowels(String[] mws) {
        Phoneme[] vowels = new Phoneme[mws.length];
        for (int i = 0; i < mws.length; i++) {
            Phoneme[] phonemes = Phoneme.find(mws[i]);
            vowels[i] = Arrays.stream(phonemes).filter(Phoneme::isVowel).findFirst().orElse(null);
        }
        return vowels;
    }


}
