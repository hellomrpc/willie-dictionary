package com.willie.dictionary.common;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Setter
@Getter
public class DicItem {
    private String word;
    private String[] mws;
    private Phoneme[] vowels;
    private Phoneme[][] phonemes;
    private String meaning;

}
