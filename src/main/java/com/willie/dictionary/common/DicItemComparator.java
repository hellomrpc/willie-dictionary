package com.willie.dictionary.common;

import java.util.Comparator;

public class DicItemComparator implements Comparator<DicItem> {

    @Override
    public int compare(DicItem o1, DicItem o2) {
        Phoneme[] vowels1 = o1.getVowels();
        Phoneme[] vowels2 = o2.getVowels();
        if(vowels1.length != vowels2.length)
            return Integer.compare(vowels1.length, vowels2.length);
        for (int i = 0; i < vowels1.length; i++) {
            Phoneme vowels1i = vowels1[i];
            Phoneme vowels2i = vowels2[i];
            if(vowels1i == null && vowels2i == null)
                return 0;
            else if(vowels1i != null && vowels2i == null)
                return 1;
            else if (vowels1i == null)
                return -1;
            else {
                if(vowels1i.getOrder() != vowels2i.getOrder())
                    return Integer.compare(vowels1i.getOrder(), vowels2i.getOrder());
            }
        }

        Phoneme[][] phonemes1 = o1.getPhonemes();
        Phoneme[][] phonemes2 = o2.getPhonemes();
        for (int i = 0; i < phonemes1.length; i++) {
            Phoneme[] phonemes1i = phonemes1[i];
            Phoneme[] phonemes2i = phonemes2[i];
            if (phonemes1i.length != phonemes2i.length)
                return Integer.compare(phonemes1i.length, phonemes2i.length);
            for (int j = 0; j < phonemes1i.length; j++) {
                Phoneme phoneme1ij = phonemes1i[j];
                Phoneme phoneme2ij = phonemes2i[j];
                if(phoneme1ij == null && phoneme2ij == null)
                    return 0;
                else if(phoneme1ij != null && phoneme2ij == null)
                    return 1;
                else if (phoneme1ij == null)
                    return -1;
                else {
                    if (phoneme1ij.getOrder() != phoneme2ij.getOrder())
                        return Integer.compare(phoneme1ij.getOrder(), phoneme2ij.getOrder());
                }
            }
        }
        return 0;
    }
}
