package com.willie.dictionary.common;

import lombok.Getter;

@Getter
public enum Consonants {

    B("b", 1),
    P("p", 2),
    D("d", 3),
    T("t", 4),
    G("g", 5),
    K("k", 6),
    F("f", 7),
    V("v", 8),
    M("m", 9),
    N1("n", 10),
    N2("ŋ", 11),
    H("h", 12),
    L("l", 13),
    J("j", 14),
    CH("ch", 15),
    SH("sh", 16),
    ZH("zh", 17),
    S("s", 18),
    Z("z", 19),
    TH1("th", 20),
    TH2("t\u035fh", 21),
    Y("y", 22),
    W("w", 23),
    R("r", 24),;

    private CharSequence phoneme;
    private int order;
    Consonants(CharSequence phoneme, int order) {
        this.phoneme = phoneme;
        this.order = order;
    }
}
