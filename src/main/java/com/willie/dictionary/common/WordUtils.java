package com.willie.dictionary.common;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.willie.dictionary.entity.Word;

import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordUtils {

    public static String parseMw(Word word) throws Exception {
        ObjectMapper om = new ObjectMapper();
        try {
            JsonNode webster = om.readTree(word.getWebster());
            if(!webster.isArray())
                throw new Exception("Webster' root is not a array");
            Iterator<JsonNode> elements = webster.elements();
            if(!elements.hasNext())
                throw new Exception("Webster' root is an empty array");
            return elements.next().path("hwi").path("prs").findPath("mw").asText();
        } catch (IOException e) {
            throw new Exception("Webster's structure is invalid, parameter", e);
        }
    }

    public static String[] splitMw(String mw) {
        final Pattern pattern = Pattern.compile("\\(.*?\\)");
        Matcher matcher = pattern.matcher(mw);
        while(matcher.find()) {
            String inBracket = matcher.group();
            if(inBracket.contains("-"))
                mw = mw.replace(inBracket, inBracket.substring(1, inBracket.length() - 1));
            else
                mw = mw.replace(inBracket, "");
        }
        return mw.split("-");
    }
}
