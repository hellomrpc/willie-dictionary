package com.willie.dictionary.controller;

import com.willie.dictionary.businessImpl.WordBusinessImpl;
import com.willie.dictionary.common.ResultBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/lookup")
public class LookupController {

    @Resource
    private WordBusinessImpl wordBusiness;



    @GetMapping("/webster")
    public ResultBean<String> webster() {
        wordBusiness.lookupWebster();
        return new ResultBean<String>().setCode(ResultBean.SUCCESS);
    }

    @GetMapping("/collins")
    public ResultBean<String> collins() {
        wordBusiness.lookupCollins();
        return new ResultBean<String>().setCode(ResultBean.SUCCESS);
    }
}
