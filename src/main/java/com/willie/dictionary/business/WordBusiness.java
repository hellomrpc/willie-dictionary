package com.willie.dictionary.business;

import com.baomidou.mybatisplus.extension.service.IService;
import com.willie.dictionary.entity.Word;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author willie
 * @since 2020-02-15
 */
public interface WordBusiness extends IService<Word> {

}
